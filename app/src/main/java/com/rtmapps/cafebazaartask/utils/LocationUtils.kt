package com.rtmapps.cafebazaartask.utils

import android.content.Context
import android.location.Location
import androidx.core.content.edit
import com.rtmapps.cafebazaartask.R
import com.rtmapps.cafebazaartask.repository.database.PrefManager.Companion.PREF_KEY_FOREGROUND_ENABLED


/**
 * Returns the `location` object as a human readable string.
 */
fun Location?.toText():String {
    return if (this != null) {
        "($latitude, $longitude)"
    } else {
        "Unknown location"
    }
}