package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Groups(
    @SerializedName("type") val type: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("items") val items: List<Items>?,
): Parcelable