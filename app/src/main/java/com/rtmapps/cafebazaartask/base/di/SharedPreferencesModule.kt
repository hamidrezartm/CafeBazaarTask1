package com.rtmapps.cafebazaartask.base.di

import com.rtmapps.cafebazaartask.repository.database.PrefManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val sharedPreferenceModule = module {
    single {
        PrefManager(androidContext())
    }
}