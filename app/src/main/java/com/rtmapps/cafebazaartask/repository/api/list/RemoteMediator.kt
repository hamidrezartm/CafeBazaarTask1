package com.rtmapps.cafebazaartask.repository.api.list

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.pojo.RemoteKey
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import com.rtmapps.cafebazaartask.repository.database.PrefManager
import com.rtmapps.cafebazaartask.repository.room.AppDatabase
import com.rtmapps.cafebazaartask.rest.WebService
import com.rtmapps.cafebazaartask.ui.activity.main.TAG
import kotlinx.coroutines.flow.collect
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.io.InvalidObjectException


@OptIn(ExperimentalPagingApi::class)
class RemoteMediator(
    private val appDatabase: AppDatabase,
    private val prefManager: PrefManager,
    private val webService: WebService
): RemoteMediator<Int, Venue>() {
    private val tag = RemoteMediator::class.java.simpleName

    private val venueDao = appDatabase.venueDao
    private val remoteKeyDao = appDatabase.remoteKeyDao

    override suspend fun load(loadType: LoadType, state: PagingState<Int, Venue>): MediatorResult {

        Log.e(tag, "RemoteMediator")

        if (prefManager.getLocation() == null || (loadType == LoadType.REFRESH))
            return MediatorResult.Success(
                endOfPaginationReached = false
            )

        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> {

                    prefManager.setRefreshPermission(false)

                    null
                }
                LoadType.PREPEND -> {
                    return MediatorResult.Success(endOfPaginationReached = true)
                }
                LoadType.APPEND -> {

                    Log.e(tag, "APPEND")

                    var remoteKey = appDatabase.withTransaction {
                        remoteKeyDao.remoteKey()
                    }

                    if (remoteKey == null)
                        remoteKey = RemoteKey(0, 1)

                    if (remoteKey.nextKey == null) {
                        throw InvalidObjectException("Remote key should not be null for $loadType")
                    }

                    remoteKey
                }
            }

            var prevPage: Int? = null

            if (loadKey?.nextKey != null) prevPage = loadKey.nextKey - 1

            val nextPage = loadKey?.nextKey ?: 1

            val response = webService.fetchVenueAsync(
                ll = prefManager.getLocation()!!,
                offset = nextPage
            ).await().body()!!

            if (response.response!!.groups!![0].items!!.isNotEmpty()) {

                val list = ArrayList<Venue>()

                for (item in response.response.groups!![0].items!!)
                    list.add(item.venue!!)


                appDatabase.withTransaction {

                    if (loadType == LoadType.REFRESH) {
                        remoteKeyDao.deleteKey()
                        venueDao.clearAll()
                    }

                    remoteKeyDao.insertOrReplace(
                        RemoteKey(
                            nextPage,
                            nextPage + 1,
                        )
                    )

                    venueDao.insertAll(list.toList())

                }

            } else {
                appDatabase.withTransaction {
                    remoteKeyDao.insertOrReplace(
                        RemoteKey(
                            prevPage,
                            null,
                        )
                    )
                }
            }


            MediatorResult.Success(
                endOfPaginationReached = response.response.groups!![0].items!!.isEmpty()
            )

        } catch (e: IOException) {
            Log.e(tag, "ERROR 1: ${e.localizedMessage}")
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            Log.e(tag, "ERROR 2: ${e.localizedMessage}")
            MediatorResult.Error(e)
        }

    }
}