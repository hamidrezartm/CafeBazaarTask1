package com.rtmapps.cafebazaartask.ui.activity.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.rtmapps.cafebazaartask.databinding.ActivitySplashBinding
import com.rtmapps.cafebazaartask.ui.activity.main.MainActivity
import kotlinx.coroutines.delay


class SplashActivity : AppCompatActivity() {

    private lateinit var ui: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(ui.root)

        lifecycleScope.launchWhenCreated {
            delay(2000)
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }

    }
}