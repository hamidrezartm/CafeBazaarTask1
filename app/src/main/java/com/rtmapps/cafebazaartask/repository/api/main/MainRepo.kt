package com.rtmapps.cafebazaartask.repository.api.main

import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.repository.database.PrefManager
import com.rtmapps.cafebazaartask.repository.room.AppDatabase
import kotlinx.coroutines.flow.Flow

class MainRepo(
    appDatabase: AppDatabase,
    private val prefManager: PrefManager,
) {
    private val locationCoordinateDao = appDatabase.locationCoordinateDao

    fun setForegroundEnable(flag: Boolean) {
        prefManager.setForegroundEnable(flag)
    }

    fun isForegroundEnable(): Boolean = prefManager.isForegroundEnable()

    suspend fun insertOrUpdateLocation(locationCoordinate: LocationCoordinate) {

        prefManager.setLocation("${locationCoordinate.latitude}, ${locationCoordinate.longitude}")
        prefManager.setRefreshPermission(true)
        locationCoordinateDao.updateLocation(locationCoordinate)
        locationCoordinateDao.insertOrReplace(locationCoordinate)

    }

    fun getLocation(): Flow<LocationCoordinate?> = locationCoordinateDao.locationCoordinate()

}