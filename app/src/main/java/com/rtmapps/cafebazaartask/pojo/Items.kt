package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.rtmapps.cafebazaartask.pojo.reasons.Reasons
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "items_table")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Parcelize
data class Items(
    @ColumnInfo(name = "referralItemsId") @SerializedName("referralId") val referralId: String?,
    @SerializedName("reasons")
    @Embedded
    val reasons: Reasons?,
    @SerializedName("venue")
    @Embedded
    val venue: Venue?,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id")
    var id: Int = 1
}
