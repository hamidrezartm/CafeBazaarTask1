package com.rtmapps.cafebazaartask.pojo.venue

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomWarnings
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "labeledLatLngs_table")
@Parcelize
data class LabeledLatLngs(
    @SerializedName("label") val label: String?,
    @SerializedName("lat") val lat: String?,
    @SerializedName("lng") val lng: String?,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "labeledId")
    var id: Int = 1
}

