package com.rtmapps.cafebazaartask.base.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rtmapps.cafebazaartask.rest.WebService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://api.foursquare.com/v2/venues/"

val restModule = module {
    factory { provideApi(get()) }
    single { provideRetrofit() }
}


fun provideRetrofit(): Retrofit {

    val gson: Gson = GsonBuilder()
        .setLenient()
        .create()

    return Retrofit
        .Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

fun provideApi(retrofit: Retrofit): WebService = retrofit.create(WebService::class.java)