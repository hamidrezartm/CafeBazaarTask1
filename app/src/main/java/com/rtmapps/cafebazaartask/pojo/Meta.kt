package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Meta(
    @SerializedName("code") val code: Int,
    @SerializedName("requestId") val requestId: String,
): Parcelable