package com.rtmapps.cafebazaartask.base.di

import com.rtmapps.cafebazaartask.repository.api.list.ListRepo
import com.rtmapps.cafebazaartask.repository.api.main.MainRepo
import org.koin.dsl.module

val repositoryModule = module {

    single {
        MainRepo(get(), get())
    }
    single {
        ListRepo(get(), get(), get())
    }

}