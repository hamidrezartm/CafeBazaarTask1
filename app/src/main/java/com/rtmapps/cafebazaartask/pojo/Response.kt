package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Response(
    @SerializedName("suggestedRadius") val suggestedRadius: Int?,
    @SerializedName("headerLocation") val headerLocation: String?,
    @SerializedName("headerFullLocation") val headerFullLocation: String?,
    @SerializedName("headerLocationGranularity") val headerLocationGranularity: String?,
    @SerializedName("totalResults") val totalResults: Int?,
    @SerializedName("groups") val groups: List<Groups>?,
): Parcelable