package com.rtmapps.cafebazaartask.ui.fragment.list.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.rtmapps.cafebazaartask.R
import com.rtmapps.cafebazaartask.databinding.LayoutNetworkStateBinding

class NetworkStateItemViewHolder(
    parent: ViewGroup,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.layout_network_state, parent, false)
) {

    private val binding = LayoutNetworkStateBinding.bind(itemView)

    private val progressBar = binding.progressBarNetworkState
    private val retry = binding.btnNetworkStateRetry
        .also {
            it.setOnClickListener { retryCallback() }
        }


    fun bindTo(loadState: LoadState) {

        progressBar.isVisible = loadState is LoadState.Loading
        retry.isVisible = loadState is LoadState.Error
    }
}
