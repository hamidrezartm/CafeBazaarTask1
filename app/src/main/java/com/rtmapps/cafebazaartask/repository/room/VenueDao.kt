package com.rtmapps.cafebazaartask.repository.room

import androidx.paging.PagingSource
import androidx.room.*
import com.rtmapps.cafebazaartask.pojo.venue.Venue

@Dao
interface VenueDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(venues: List<Venue>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(venue: Venue)

    @Query("SELECT * FROM venue_table ORDER BY venueId ASC")
    fun pagingSource(): PagingSource<Int, Venue>

    @Query("DELETE FROM venue_table")
    fun clearAll()

    @Update(entity = Venue::class)
    fun updateVenue(venue: Venue)

}