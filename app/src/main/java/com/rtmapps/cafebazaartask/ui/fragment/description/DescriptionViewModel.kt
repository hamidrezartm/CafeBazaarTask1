package com.rtmapps.cafebazaartask.ui.fragment.description

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import java.lang.Exception

class DescriptionViewModel : ViewModel() {

    private var _venue = MutableLiveData<Venue>()
    val venue: LiveData<Venue> = _venue

    fun setPostData(venue: Venue) {
        _venue.postValue(venue)
    }

    fun mapDirection(activity: Activity) {

        try {

            val gmmIntentUri =  Uri.parse("geo:${venue.value!!.location!!.lat},${venue.value!!.location!!.lng}?q=" + Uri.encode("${venue.value!!.name}"))
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            activity.startActivity(mapIntent)

        } catch (e: Exception) {

            Toast.makeText(activity, "Can't Direct to location!", Toast.LENGTH_SHORT).show()

        }

    }

}