package com.rtmapps.cafebazaartask.base

import android.app.Application
import com.rtmapps.cafebazaartask.base.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BaseApplication)
            modules(sharedPreferenceModule, roomModule, restModule, repositoryModule, viewModelModule)
        }

    }


}