package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "location_coordinate_table")
@Parcelize
data class LocationCoordinate(
    val latitude: Double,
    val longitude: Double,
    val name: String,
): Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}