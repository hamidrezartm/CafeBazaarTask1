package com.rtmapps.cafebazaartask.ui.activity.main

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.rtmapps.cafebazaartask.BuildConfig
import com.rtmapps.cafebazaartask.R
import com.rtmapps.cafebazaartask.databinding.ActivityMainBinding
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.service.ForegroundOnlyLocationService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

const val TAG = "MainActivity"
private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34

class MainActivity : AppCompatActivity(), OnMapReadyCallback {


    private lateinit var ui: ActivityMainBinding

    private val viewModel: MainViewModel by viewModel()

    private var mMap: GoogleMap? = null

    private var foregroundOnlyLocationServiceBound = false

    // Provides location updates for while-in-use feature.
    private var foregroundOnlyLocationService: ForegroundOnlyLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    private lateinit var foregroundOnlyBroadcastReceiver: ForegroundOnlyBroadcastReceiver

    // Monitors connection to the while-in-use service.
    private val foregroundOnlyServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as ForegroundOnlyLocationService.LocalBinder
            foregroundOnlyLocationService = binder.service
            foregroundOnlyLocationServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            foregroundOnlyLocationService = null
            foregroundOnlyLocationServiceBound = false
        }
    }

    private var savedLocation: LocationCoordinate? = null

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = ActivityMainBinding.inflate(layoutInflater)
        setContentView(ui.root)
        setSupportActionBar(ui.toolbar)
        ui.mapView.onCreate(savedInstanceState)
        ui.mapView.getMapAsync(this)

        setupObservers()

        foregroundOnlyBroadcastReceiver = ForegroundOnlyBroadcastReceiver()

        if (!viewModel.isForegroundEnable()) {
            if (foregroundPermissionApproved()) {
                lifecycleScope.launch {
                    delay(500)
                    foregroundOnlyLocationService?.subscribeToLocationUpdates()
                        ?: Log.d(TAG, "Service Not Bound")
                }

            } else {
                requestLocationPermission()
            }
        } else {
            foregroundOnlyLocationService?.unsubscribeToLocationUpdates()
            if (foregroundPermissionApproved()) {
                lifecycleScope.launch {
                    delay(500)
                    foregroundOnlyLocationService?.subscribeToLocationUpdates()
                        ?: Log.d(TAG, "Service Not Bound")
                }

            } else {
                requestLocationPermission()
            }
        }

    }

    @ExperimentalCoroutinesApi
    private fun setupObservers() {

        lifecycleScope.launchWhenCreated {
            viewModel.location.collectLatest {
                savedLocation = it
                if (it != null)
                    moveMapCamera(it)
            }
        }


    }

    override fun onStart() {
        super.onStart()
        ui.mapView.onStart()
        val serviceIntent = Intent(this, ForegroundOnlyLocationService::class.java)
        bindService(
            serviceIntent,
            foregroundOnlyServiceConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    override fun onResume() {
        super.onResume()
        ui.mapView.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            foregroundOnlyBroadcastReceiver,
            IntentFilter(
                ForegroundOnlyLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST
            )
        )
    }

    override fun onPause() {
        ui.mapView.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            foregroundOnlyBroadcastReceiver
        )
        super.onPause()
    }

    override fun onStop() {
        ui.mapView.onStop()
        if (foregroundOnlyLocationServiceBound) {
            unbindService(foregroundOnlyServiceConnection)
            foregroundOnlyLocationServiceBound = false
        }
        super.onStop()
    }

    @ExperimentalCoroutinesApi
    override fun onDestroy() {
        ui.mapView.onDestroy()
        if (viewModel.isForegroundEnable()) {
            foregroundOnlyLocationService?.unsubscribeToLocationUpdates()
        }
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        ui.mapView.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        mMap!!.uiSettings.setAllGesturesEnabled(false)
    }

    private fun requestLocationPermission() {

        val provideRationale = foregroundPermissionApproved()
        // If the user denied a previous request, but didn't check "Don't ask again", provide
        // additional rationale.
        if (provideRationale) {
            Snackbar.make(
                ui.root,
                R.string.permission_rationale,
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.ok) {
                    // Request permission
                    ActivityCompat.requestPermissions(
                        this@MainActivity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
                    )
                }
                .show()
        } else {
            Log.d(TAG, "Request foreground only permission")
            ActivityCompat.requestPermissions(
                this@MainActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.d(TAG, "onRequestPermissionResult")

        when (requestCode) {
            REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE -> when {
                grantResults.isEmpty() ->
                    // If user interaction was interrupted, the permission request
                    // is cancelled and you receive empty arrays.
                    Log.d(TAG, "User interaction was cancelled.")

                grantResults[0] == PackageManager.PERMISSION_GRANTED ->
                    // Permission was granted.
                    foregroundOnlyLocationService?.subscribeToLocationUpdates()

                else -> {

                    Snackbar.make(
                        ui.root,
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_LONG
                    )
                        .setAction(R.string.settings) {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID,
                                null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                        .show()
                }
            }
        }
    }

    private fun foregroundPermissionApproved(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    /**
     * Receiver for location broadcasts from [ForegroundOnlyLocationService].
     */

    private inner class ForegroundOnlyBroadcastReceiver : BroadcastReceiver() {

        @ExperimentalCoroutinesApi
        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getParcelableExtra<Location>(
                ForegroundOnlyLocationService.EXTRA_LOCATION
            )

            if (location != null) {

                if (savedLocation != null) {

                    // Calculating Location Distance
                    val locationA = Location("point A")
                    locationA.latitude = savedLocation!!.latitude
                    locationA.longitude = savedLocation!!.longitude

                    if (locationA.distanceTo(location) > 100) {
                        handleGettingLocation(location)
                    }

                } else {
                    handleGettingLocation(location)
                }

            }
        }
    }

    @ExperimentalCoroutinesApi
    private fun handleGettingLocation(location: Location) {

        Toast.makeText(this@MainActivity, "UPDATING LOCATION", Toast.LENGTH_SHORT).show()

        var city = ""

        try {

            val locale = Locale("fa_")
            val geocoder = Geocoder(this@MainActivity, locale)

            val addresses =
                geocoder.getFromLocation(location.latitude, location.longitude, 1)
            city = addresses[0].locality

        } catch (e: Exception) {

        }

        viewModel.insertOrUpdateLocation(
            LocationCoordinate(
                location.latitude,
                location.longitude,
                city
            )
        )

    }

    private fun moveMapCamera(locationCoordinate: LocationCoordinate) {

        val location = LatLng(
            locationCoordinate.latitude,
            locationCoordinate.longitude
        )

        mMap!!.clear()

        mMap!!.addMarker(
            MarkerOptions()
                .position(location)
                .title(locationCoordinate.name)
        )
        mMap!!.setMinZoomPreference(15F)
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(location))

    }

}
