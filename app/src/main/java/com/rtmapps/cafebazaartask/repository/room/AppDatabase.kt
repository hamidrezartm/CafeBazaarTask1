package com.rtmapps.cafebazaartask.repository.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rtmapps.cafebazaartask.pojo.Items
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.pojo.RemoteKey
import com.rtmapps.cafebazaartask.pojo.reasons.Reasons
import com.rtmapps.cafebazaartask.pojo.reasons.ReasonsItems
import com.rtmapps.cafebazaartask.pojo.venue.*

@Database(
    entities = [Items::class, Reasons::class, ReasonsItems::class, Venue::class, Categories::class, Icon::class, RemoteKey::class, LocationCoordinate::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    Converters::class
)
abstract class AppDatabase : RoomDatabase() {
    abstract val venueDao: VenueDao
    abstract val remoteKeyDao: RemoteKeyDao
    abstract val locationCoordinateDao: LocationCoordinateDao
}