package com.rtmapps.cafebazaartask.repository.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rtmapps.cafebazaartask.pojo.RemoteKey

@Dao
interface RemoteKeyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplace(remoteKey: RemoteKey)

    @Query("SELECT * FROM remoteKey_table ORDER BY id DESC")
    suspend fun remoteKey(): RemoteKey?

    @Query("DELETE FROM remoteKey_table")
    suspend fun deleteKey()
}