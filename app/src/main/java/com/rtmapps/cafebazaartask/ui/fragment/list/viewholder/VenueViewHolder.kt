package com.rtmapps.cafebazaartask.ui.fragment.list.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.rtmapps.cafebazaartask.databinding.LayoutItemPlaceBinding
import com.rtmapps.cafebazaartask.pojo.venue.Venue

class VenueViewHolder(
    private val ui: LayoutItemPlaceBinding,
    private val onItemClickListener: (LayoutItemPlaceBinding, Venue) -> Unit
): RecyclerView.ViewHolder(ui.root) {

    fun bind(venue: Venue?) {

        if (venue == null)
            return

        ViewCompat.setTransitionName(ui.cvItemPlace, "cv_${venue.venueIdd}")

        ui.txtItemPlaceTitle.text = venue.name
        ui.txtItemPlaceDescription.text = venue.location!!.address

        ui.root.setOnClickListener { onItemClickListener(ui, venue) }

    }

    companion object {
        fun create(
            parent: ViewGroup,
            onItemClickListener: (LayoutItemPlaceBinding, Venue) -> Unit
        ): VenueViewHolder {

            val binding = LayoutItemPlaceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            return VenueViewHolder(binding, onItemClickListener)
        }
    }


}