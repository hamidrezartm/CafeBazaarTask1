package com.rtmapps.cafebazaartask.ui.activity.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.repository.api.main.MainRepo
import kotlinx.coroutines.launch

class MainViewModel(
    private val mainRepo: MainRepo
): ViewModel() {

    val location = mainRepo.getLocation()

    fun setForegroundEnable(flag: Boolean) {
        mainRepo.setForegroundEnable(flag)
    }
    fun isForegroundEnable(): Boolean = mainRepo.isForegroundEnable()

    fun insertOrUpdateLocation(locationCoordinate: LocationCoordinate) {

        viewModelScope.launch {
            mainRepo.insertOrUpdateLocation(locationCoordinate)
        }

    }

}