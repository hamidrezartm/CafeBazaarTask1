package com.rtmapps.cafebazaartask.ui.fragment.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.rtmapps.cafebazaartask.repository.api.list.ListRepo

class ListViewModel(
    private val listRepo: ListRepo
) : ViewModel() {

    val location = listRepo.getLocation()

    fun setRefreshPermission(refresh: Boolean) {
        listRepo.setRefreshPermission(refresh)
    }

    fun fetchVenue() = listRepo.fetchVenue().cachedIn(viewModelScope)

}