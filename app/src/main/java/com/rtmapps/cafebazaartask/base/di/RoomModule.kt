package com.rtmapps.cafebazaartask.base.di

import android.app.Application
import androidx.room.Room
import com.rtmapps.cafebazaartask.repository.room.AppDatabase
import com.rtmapps.cafebazaartask.repository.room.LocationCoordinateDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

const val DATABASE_NAME = "database"


val roomModule = module {

    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideLocationCoordinateDao(database: AppDatabase): LocationCoordinateDao {
        return  database.locationCoordinateDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideLocationCoordinateDao(get()) }
}