package com.rtmapps.cafebazaartask.repository.database

import android.content.Context
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.rtmapps.cafebazaartask.R

class  PrefManager(
    context: Context
) {

    private val keyGenParameterSpec = KeyGenParameterSpec.Builder(
        MASTER_KEY_ALIAS,
        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
    ).setBlockModes(KeyProperties.BLOCK_MODE_GCM)
        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
        .setKeySize(KEY_SIZE)
        .build()

    private val masterKeyAlias = MasterKey.Builder(context, MASTER_KEY_ALIAS)
        .setKeyGenParameterSpec(keyGenParameterSpec)
        .build()

    private val pref = EncryptedSharedPreferences.create(
        context,
        context.getString(R.string.app_name),
        masterKeyAlias,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    // Editor for Shared preferences
    private var editor = pref.edit()

    fun setForegroundEnable(flag: Boolean) {
        editor.putBoolean(PREF_KEY_FOREGROUND_ENABLED, flag)
        editor.commit()
    }

    fun isForegroundEnable(): Boolean {
        return pref.getBoolean(PREF_KEY_FOREGROUND_ENABLED, false)
    }

    fun setRefreshPermission(flag: Boolean) {
        editor.putBoolean(PREF_KEY_REFRESH, flag)
        editor.commit()
    }

    fun canRefresh(): Boolean {
        return pref.getBoolean(PREF_KEY_REFRESH, true)
    }

    fun setLocation(location: String) {
        editor.putString(PREF_KEY_LOCATION, location)
        editor.commit()
    }

    fun getLocation(): String? {
        return pref.getString(PREF_KEY_LOCATION, null)
    }

    companion object {
        const val MASTER_KEY_ALIAS = "AndroidxMasterAlias"
        const val KEY_SIZE = 256
        const val PREF_KEY_FOREGROUND_ENABLED = "prefKeyForegroundEnable"
        const val PREF_KEY_REFRESH = "prefKeyRefresh"
        const val PREF_KEY_LOCATION = "prefKeyLocation"
    }
}