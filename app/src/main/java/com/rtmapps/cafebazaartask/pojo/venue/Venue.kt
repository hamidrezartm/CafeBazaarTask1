package com.rtmapps.cafebazaartask.pojo.venue

import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "venue_table")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Parcelize
data class Venue(
    @ColumnInfo(name = "venueIdServer") @SerializedName("id") val venueId: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("location")
    @Embedded val location: Location?,
    @SerializedName("categories") val categories: List<Categories>?,
    @SerializedName("photos")
    @Embedded val photos: Photos?,
    @ColumnInfo(name = "referralVenueId") @SerializedName("referralId") val referralId: String?,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "venueId")
    var venueIdd: Int = 1
}