package com.rtmapps.cafebazaartask.pojo.reasons

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomWarnings
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "reasons_item_table")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Parcelize
data class ReasonsItems(
    @SerializedName("summary") val summary: String,
    @SerializedName("type") val type: String,
    @SerializedName("reasonName") val reasonName: String,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "reasonsItemsId")
    var id: Int = 1
}
