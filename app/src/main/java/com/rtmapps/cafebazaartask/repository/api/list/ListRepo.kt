package com.rtmapps.cafebazaartask.repository.api.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import com.rtmapps.cafebazaartask.repository.database.PrefManager
import com.rtmapps.cafebazaartask.repository.room.AppDatabase
import com.rtmapps.cafebazaartask.rest.WebService
import kotlinx.coroutines.flow.Flow

class ListRepo(
    private val appDatabase: AppDatabase,
    private val prefManager: PrefManager,
    private val webService: WebService,
) {

    private val locationCoordinateDao = appDatabase.locationCoordinateDao
    private val venueDao = appDatabase.venueDao

    fun setRefreshPermission(permission: Boolean) {

        prefManager.setRefreshPermission(permission)

    }

    fun getLocation(): Flow<LocationCoordinate?> = locationCoordinateDao.locationCoordinate()

    fun fetchVenue(): Flow<PagingData<Venue>> {

        val pagingSourceFactory = { venueDao.pagingSource() }

        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = RemoteMediator(appDatabase, prefManager, webService),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 15
    }

}

