package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class FoursquareResponse(
    @SerializedName("meta") val meta: Meta?,
    @SerializedName("response") val response: Response?,
): Parcelable
