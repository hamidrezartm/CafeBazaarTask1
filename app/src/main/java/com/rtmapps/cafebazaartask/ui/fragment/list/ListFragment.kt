package com.rtmapps.cafebazaartask.ui.fragment.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.rtmapps.cafebazaartask.R
import com.rtmapps.cafebazaartask.databinding.FragmentListBinding
import com.rtmapps.cafebazaartask.databinding.LayoutItemPlaceBinding
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import com.rtmapps.cafebazaartask.ui.fragment.list.adapter.LoadStateAdapter
import com.rtmapps.cafebazaartask.ui.fragment.list.adapter.VenueAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import org.koin.androidx.viewmodel.ext.android.viewModel


private const val TAG = "ListFragment"

class ListFragment : Fragment() {

    private var _ui: FragmentListBinding? = null
    private val ui get() = _ui!!

    @ExperimentalCoroutinesApi
    private val viewModel: ListViewModel by viewModel()

    private val onItemClickListener: (LayoutItemPlaceBinding, Venue) -> Unit = { layoutUi, venue ->

        val bundle = Bundle()
        bundle.putParcelable("venue", venue)

        val extras = FragmentNavigatorExtras(
            layoutUi.cvItemPlace to "cv_${venue.venueIdd}"
        )

        layoutUi.root.findNavController().navigate(
            R.id.action_listFragment_to_descriptionFragment,
            bundle,
            NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_bottom)
                .setExitAnim(R.anim.nav_default_exit_anim)
                .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
                .setPopExitAnim(R.anim.slide_out_bottom)
                .build(),
            extras
        )

    }

    @ExperimentalCoroutinesApi
    private val venueAdapter by lazy { VenueAdapter(onItemClickListener) }
    private lateinit var layoutManager: LinearLayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragmentListBinding.inflate(inflater, container, false)
        return ui.root
    }

    @ExperimentalCoroutinesApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initView()
        setupRvObserver()
        setupObservers()

    }

    @ExperimentalCoroutinesApi
    private fun initView() {

        layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        ui.rvVenue.layoutManager = layoutManager
        ui.rvVenue.adapter = venueAdapter.withLoadStateFooter(footer = LoadStateAdapter(venueAdapter))
        ui.rvVenue.apply {
            postponeEnterTransition()
            viewTreeObserver
                .addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }
        }

        ui.swipeRefreshLayout.setOnRefreshListener {
            viewModel.setRefreshPermission(true)
            venueAdapter.refresh()
            Snackbar.make(
                ui.root,
                "Fetching Venues",
                Snackbar.LENGTH_LONG
            ).show()
        }

    }

    @ExperimentalCoroutinesApi
    private fun setupObservers() {

        // For Location changes
        lifecycleScope.launchWhenCreated {
            viewModel.location.collectLatest {
                if (it != null) venueAdapter.refresh()
            }
        }

    }

    @ExperimentalCoroutinesApi
    private fun setupRvObserver() {

        lifecycleScope.launchWhenCreated {
            viewModel.fetchVenue().collectLatest {
                try {
                    if (ui.swipeRefreshLayout.isRefreshing) ui.swipeRefreshLayout.isRefreshing =
                        false
                } catch (e: java.lang.NullPointerException) {

                } finally {
                    venueAdapter.submitData(it)
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            @OptIn(FlowPreview::class)
            venueAdapter.loadStateFlow
                // Only emit when REFRESH LoadState for RemoteMediator changes.
                .distinctUntilChangedBy { it.refresh }
                // Only react to cases where Remote REFRESH completes i.e., NotLoading.
                .filter { it.refresh is LoadState.NotLoading }
                .collect { ui.rvVenue.scrollToPosition(0) }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _ui = null
    }

}