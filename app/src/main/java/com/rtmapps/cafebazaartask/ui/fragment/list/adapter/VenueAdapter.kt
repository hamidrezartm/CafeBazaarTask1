package com.rtmapps.cafebazaartask.ui.fragment.list.adapter

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.rtmapps.cafebazaartask.databinding.LayoutItemPlaceBinding
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import com.rtmapps.cafebazaartask.ui.fragment.list.viewholder.VenueViewHolder

class VenueAdapter(
    private val onItemClickListener: (LayoutItemPlaceBinding, Venue) -> Unit
): PagingDataAdapter<Venue, VenueViewHolder>(VENUE_COMPARATOR) {

    private var viewHolder: VenueViewHolder? = null

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder {
        viewHolder = VenueViewHolder.create(parent, onItemClickListener)
        return viewHolder!!
    }

    /**
     * Viewholder class for [VenueAdapter]
     *
     * @constructor
     * receives a [View]
     *
     * @param itemView
     */

    companion object {
        val VENUE_COMPARATOR = object : DiffUtil.ItemCallback<Venue>() {
            override fun areContentsTheSame(oldItem: Venue, newItem: Venue): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: Venue, newItem: Venue): Boolean =
                oldItem.venueIdd == newItem.venueIdd
        }

    }

}