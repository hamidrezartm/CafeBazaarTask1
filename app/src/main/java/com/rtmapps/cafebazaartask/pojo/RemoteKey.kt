package com.rtmapps.cafebazaartask.pojo

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomWarnings
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "remoteKey_table")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Parcelize
data class RemoteKey(
    @ColumnInfo(name = "remote_prev_key") val prevKey: Int?,
    @ColumnInfo(name = "remote_next_key") val nextKey: Int?
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id")
    var id: Int = 1
}
