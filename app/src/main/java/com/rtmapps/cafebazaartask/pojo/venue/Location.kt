package com.rtmapps.cafebazaartask.pojo.venue

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomWarnings
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "location_table")
@Parcelize
data class Location(
    @SerializedName("address") val address: String?,
    @SerializedName("crossStreet") val crossStreet: String?,
    @SerializedName("lat") val lat: String?,
    @SerializedName("lng") val lng: String?,
    @SerializedName("labeledLatLngs") val labeledLatLngs: List<LabeledLatLngs?>?,
    @SerializedName("distance") val distance: Int?,
    @SerializedName("postalCode") val postalCode: String?,
    @SerializedName("cc") val cc: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state") val state: String?,
    @SerializedName("country") val country: String?,
    @SerializedName("formattedAddress") val formattedAddress: List<String>?,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "locId")
    var id: Int = 1
}
