package com.rtmapps.cafebazaartask.rest

import com.google.gson.JsonObject
import com.rtmapps.cafebazaartask.pojo.FoursquareResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface WebService {

    @GET("explore")
    fun fetchVenueAsync(
        @Query("v") v:String = "20201029",
        @Query("client_id") clientId:String = "U4ZZHHQYVTKDE13VODPZZUKV4PHTO0DH3UUCGXZHIXFCXSCY",
        @Query("client_secret") clientSecret:String = "VF3WVNBKWZ5F1XHABS0K1SH4A3TFHNUQXW4OFGB4NNQGPOSX",
        @Query("ll") ll:String,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int = 30,
    ): Deferred<Response<FoursquareResponse>>

}