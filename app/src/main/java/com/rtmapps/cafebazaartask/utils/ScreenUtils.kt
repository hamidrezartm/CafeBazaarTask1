package com.rtmapps.cafebazaartask.utils

import android.app.Activity
import android.util.DisplayMetrics

/**
 * Determines the height of status bar
 *
 * @param activity
 * @return height of device screen
 */
fun getScreenHeight(activity: Activity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}