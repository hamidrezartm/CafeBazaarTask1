package com.rtmapps.cafebazaartask.repository.room

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rtmapps.cafebazaartask.pojo.Groups
import com.rtmapps.cafebazaartask.pojo.Items
import com.rtmapps.cafebazaartask.pojo.Response
import com.rtmapps.cafebazaartask.pojo.reasons.ReasonsItems
import com.rtmapps.cafebazaartask.pojo.venue.Categories
import com.rtmapps.cafebazaartask.pojo.venue.Icon
import com.rtmapps.cafebazaartask.pojo.venue.LabeledLatLngs
import com.rtmapps.cafebazaartask.pojo.venue.Location
import java.lang.reflect.Type
import java.util.*

class Converters {

    @TypeConverter
    fun fromReasonsItemsList(reasonsItems: List<ReasonsItems?>?): String? {
        val type = object : TypeToken<List<ReasonsItems?>?>() {}.type
        return Gson().toJson(reasonsItems, type)
    }

    @TypeConverter
    fun toReasonsItemsList(reasonsItemsString: String?): List<ReasonsItems>? {
        val type = object : TypeToken<List<ReasonsItems?>?>() {}.type
        return Gson().fromJson<List<ReasonsItems>>(reasonsItemsString, type)
    }

    @TypeConverter
    fun fromCategories(categories: Categories?): String? {
        val type = object : TypeToken<Categories>() {}.type
        return Gson().toJson(categories, type)
    }

    @TypeConverter
    fun toCategories(categories: String?): Categories? {
        val type = object : TypeToken<Categories>() {}.type
        return Gson().fromJson<Categories>(categories, type)
    }

    @TypeConverter
    fun fromCategoriesItemsList(categories: List<Categories>?): String? {
        val type = object : TypeToken<List<Categories>>() {}.type
        return Gson().toJson(categories, type)
    }

    @TypeConverter
    fun toCategoriesItemsList(categories: String?): List<Categories>? {
        val type = object : TypeToken<List<Categories>>() {}.type
        return Gson().fromJson<List<Categories>>(categories, type)
    }

    @TypeConverter
    fun fromLabeledLatLngsItemsList(labeledLatLngs: List<LabeledLatLngs?>?): String? {
        val type = object : TypeToken<List<LabeledLatLngs?>?>() {}.type
        return Gson().toJson(labeledLatLngs, type)
    }

    @TypeConverter
    fun toLabeledLatLngsItemsList(labeledLatLngs: String?): List<LabeledLatLngs?>? {
        val type = object : TypeToken<List<LabeledLatLngs?>?>() {}.type
        return Gson().fromJson<List<LabeledLatLngs?>?>(labeledLatLngs, type)
    }

    @TypeConverter
    fun fromIcons(icon: Icon?): String? {
        val type = object : TypeToken<Icon>() {}.type
        return Gson().toJson(icon, type)
    }

    @TypeConverter
    fun toIcons(icon: String?): Icon? {
        val type = object : TypeToken<Icon>() {}.type
        return Gson().fromJson<Icon>(icon, type)
    }

    @TypeConverter
    fun stringListToJson(value: List<String>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToStringList(value: String) = Gson().fromJson(value, Array<String>::class.java).toList()


//    @TypeConverter
//    fun stringToList(value: String): List<String> {
//        val listType: Type = object : TypeToken<List<String>>() {}.type
//        return Gson().fromJson(value, listType)
//    }
//
//    @TypeConverter
//    fun stringListToString(list: List<String>): String {
//        val gson = Gson()
//        return gson.toJson(list)
//    }
//
//    @TypeConverter
//    fun stringToCategories(json: String): Categories {
//        val gson = Gson()
//        val type = object : TypeToken<Categories>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun categoriesToString(categories: Categories): String {
//        val gson = Gson()
//        val type = object : TypeToken<Categories>() {}.type
//        return gson.toJson(categories, type)
//    }
//
//    @TypeConverter
//    fun stringToLabeledLatLngs(json: String): LabeledLatLngs {
//        val gson = Gson()
//        val type = object : TypeToken<LabeledLatLngs>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun labeledLatLngsToString(labeledLatLngs: LabeledLatLngs): String {
//        val gson = Gson()
//        val type = object : TypeToken<LabeledLatLngs>() {}.type
//        return gson.toJson(labeledLatLngs, type)
//    }
//
//    @TypeConverter
//    fun stringLocations(json: String): Location {
//        val gson = Gson()
//        val type = object : TypeToken<Location>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun locationToString(location: Location): String {
//        val gson = Gson()
//        val type = object : TypeToken<Location>() {}.type
//        return gson.toJson(location, type)
//    }
//
//    @TypeConverter
//    fun stringItems(json: String): Items {
//        val gson = Gson()
//        val type = object : TypeToken<Items>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun itemsToString(items: Items): String {
//        val gson = Gson()
//        val type = object : TypeToken<Items>() {}.type
//        return gson.toJson(items, type)
//    }
//
//    @TypeConverter
//    fun stringGroups(json: String): Groups {
//        val gson = Gson()
//        val type = object : TypeToken<Groups>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun groupsToString(groups: Groups): String {
//        val gson = Gson()
//        val type = object : TypeToken<Groups>() {}.type
//        return gson.toJson(groups, type)
//    }
//
//    @TypeConverter
//    fun stringResponse(json: String): Response {
//        val gson = Gson()
//        val type = object : TypeToken<Response>() {}.type
//        return gson.fromJson(json, type)
//    }
//
//    @TypeConverter
//    fun responseToString(response: Response): String {
//        val gson = Gson()
//        val type = object : TypeToken<Response>() {}.type
//        return gson.toJson(response, type)
//    }
}