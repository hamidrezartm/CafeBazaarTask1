package com.rtmapps.cafebazaartask.repository.room

import androidx.room.*
import com.rtmapps.cafebazaartask.pojo.LocationCoordinate
import kotlinx.coroutines.flow.Flow

@Dao
interface LocationCoordinateDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplace(locationCoordinate: LocationCoordinate)

    @Query("SELECT * FROM location_coordinate_table ORDER BY id DESC LIMIT 1")
    fun locationCoordinate(): Flow<LocationCoordinate?>

    @Update(entity = LocationCoordinate::class)
    suspend fun updateLocation(locationCoordinate: LocationCoordinate)

    @Query("DELETE FROM location_coordinate_table")
    fun deleteLocation()
}