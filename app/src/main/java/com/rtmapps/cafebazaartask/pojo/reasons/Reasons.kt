package com.rtmapps.cafebazaartask.pojo.reasons

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.RoomWarnings
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "reasons_table")
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Parcelize
data class Reasons(
    @ColumnInfo(name = "reasonsCount") @SerializedName("count") val count: Int?,
    @SerializedName("items") val items: List<ReasonsItems>?,
): Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "reasonsId")
    var reasonsId: Int = 1
}
