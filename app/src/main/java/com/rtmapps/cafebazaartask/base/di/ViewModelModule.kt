package com.rtmapps.cafebazaartask.base.di

import com.rtmapps.cafebazaartask.ui.activity.main.MainViewModel
import com.rtmapps.cafebazaartask.ui.fragment.description.DescriptionViewModel
import com.rtmapps.cafebazaartask.ui.fragment.list.ListViewModel
import org.koin.dsl.module

val viewModelModule = module {

    single {
        MainViewModel(get())
    }
    single {
        ListViewModel(get())
    }

    single {
        DescriptionViewModel()
    }

}