package com.rtmapps.cafebazaartask.ui.fragment.description

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.transition.TransitionInflater
import com.rtmapps.cafebazaartask.databinding.FragmentDescriptionBinding
import com.rtmapps.cafebazaartask.pojo.venue.Venue
import org.koin.androidx.viewmodel.ext.android.viewModel

class DescriptionFragment : Fragment() {

    private var _ui: FragmentDescriptionBinding? = null
    private val ui get() = _ui!!

    private val viewModel: DescriptionViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setPostData(arguments?.getParcelable<Venue>("venue") as Venue)

        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _ui = FragmentDescriptionBinding.inflate(inflater, container, false)
        return ui.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui.btnDirection.setOnClickListener {
            viewModel.mapDirection(requireActivity())
        }

        val venue = arguments?.getParcelable<Venue>("venue") as Venue

        ViewCompat.setTransitionName(ui.cv, "cv_${venue.venueIdd}")

        setupObservers()

    }

    @SuppressLint("SetTextI18n")
    private fun setupObservers() {

        viewModel.venue.observe(viewLifecycleOwner, {

            ui.txtLocation.text = "${it.location!!.lat}, ${it.location.lng}"
            ui.txtName.text = it.name
            ui.txtDistance.text = "${it.location.distance} m"
            ui.txtCountry.text = it.location.country
            ui.txtCity.text = it.location.city
            ui.txtCC.text = it.location.cc
            ui.txtPostalCode.text = it.location.postalCode

            var location = ""

            if (it.location.address != null)
                location += it.location.address

            if (it.location.crossStreet != null)
                location += " - ${it.location.crossStreet}"

            ui.txtDescription.text = location


        })


    }



}